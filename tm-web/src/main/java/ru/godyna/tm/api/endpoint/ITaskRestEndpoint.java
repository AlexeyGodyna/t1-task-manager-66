package ru.godyna.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.godyna.tm.model.Task;

import java.util.Collection;

public interface ITaskRestEndpoint {

    @Nullable
    Collection<Task> findAll();

    @Nullable
    Task findById(String id);

    @NotNull
    Task save(Task task);

    void delete(Task task);

    void delete(String id);

}
