package ru.godyna.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.godyna.tm.model.Project;

import java.util.Collection;

public interface IProjectRestEndpoint {

    @Nullable
    Collection<Project> findAll();

    @Nullable
    Project findById(String id);

    @NotNull
    Project save(Project project);

    void delete(Project project);

    void delete(String id);

}
