package ru.godyna.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.godyna.tm")
public class ApplicationConfiguration {

}
