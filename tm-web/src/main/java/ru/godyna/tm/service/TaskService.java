package ru.godyna.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.godyna.tm.api.repository.ITaskRepository;
import ru.godyna.tm.model.Task;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class TaskService {

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Transactional
    public void add(@Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.save(model);
    }

    @Transactional
    public void update(@Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.save(model);
    }

    @Nullable
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Transactional
    public void remove(@Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.delete(model);
    }

    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        taskRepository.deleteById(id);
    }

    @Nullable
    public Task findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        return taskRepository.findById(id).orElse(null);
    }


}
