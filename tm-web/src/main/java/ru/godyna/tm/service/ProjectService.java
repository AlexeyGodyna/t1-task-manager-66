package ru.godyna.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.godyna.tm.api.repository.IProjectRepository;
import ru.godyna.tm.model.Project;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Transactional
    public void add(@Nullable Project model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.save(model);
    }

    @Transactional
    public void update(@Nullable Project model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.save(model);
    }

    @Nullable
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Transactional
    public void remove(@Nullable Project model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.delete(model);
    }

    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        projectRepository.deleteById(id);
    }

    @Nullable
    public Project findOneById(@Nullable String id) {
        return projectRepository.findById(id).orElse(null);
    }

}
