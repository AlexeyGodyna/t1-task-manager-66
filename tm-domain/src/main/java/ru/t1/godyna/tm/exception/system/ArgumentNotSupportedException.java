package ru.t1.godyna.tm.exception.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class ArgumentNotSupportedException extends AbstractSystemException{

    public ArgumentNotSupportedException(@NotNull final String argName) {
        super("Error! Argument '" + argName + "' is not supported...");
    }

}
