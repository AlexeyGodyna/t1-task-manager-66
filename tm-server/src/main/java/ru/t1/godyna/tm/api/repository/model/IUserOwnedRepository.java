package ru.t1.godyna.tm.api.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.godyna.tm.model.AbstractUserOwnedModel;

@Repository
@Scope("prototype")
public interface IUserOwnedRepository <M extends AbstractUserOwnedModel> extends IRepository<M> {

}
